import sys, os, logging, re

import telepot
from telepot.namedtuple import (
	InlineKeyboardMarkup, InlineKeyboardButton,
	CallbackQuery, ReplyKeyboardMarkup, KeyboardButton,
	InlineQuery, InlineQueryResultArticle
)
from IntraSession import IntraSession
from helpers import header_link_to_dict
from IntraBotRouterMixin import (
	IntraBotRouterMixin, _find_router_childs, _find_router_parent, _by_command,
	_by_text, _by_request_my, _on_text_all_routers, _check_on_back
)
from slots import filter_slots_by_days, filter_slots

import views
import emoji
streamhndlr = logging.StreamHandler(sys.stderr)
streamhndlr.setLevel(logging.INFO)
filehandler = logging.FileHandler(filename=os.path.curdir + 'info.log')
filehandler.setLevel(logging.INFO)
log = logging.getLogger(__name__)
log.addHandler(streamhndlr)
log.addHandler(filehandler)
log.setLevel(logging.INFO)

print(log)

sess = telepot.helper.SafeDict()


def _another_page_exist(
	session: IntraSession, name: str,
	link: dict,num: int, page_num: int
):
	if name in link and num:
		resp_name = session.get("/v2/me/slots", params={
			"page[number]": page_num - 1,
			"page[size]": 20
		})
		if resp_name.status_code != 200:
			print(resp_name)
			return
		prev_slots = list(filter(filter_slots, resp_name.json()))
		print("prev_slots = ", len(prev_slots))


class MessageHandler(telepot.helper.ChatHandler, IntraBotRouterMixin):
	def __init__(self, *args, **kwargs):
		super(MessageHandler, self).__init__(*args, **kwargs)
		global sess
		self.on_text_current = "on_bot_command"
		if self.id in sess:
			self.intrasess = sess[self.id]
		else:
			self.intrasess = None
		self.keyboards = dict()
		self.texts = dict()
		self.init_markups()
		self.slots_page_num = 1
		self.link = dict()

	def init_markups(self):
		self.keyboards["on_bot_command"] = ReplyKeyboardMarkup(
			keyboard=[
				[KeyboardButton(text="Request new token")]
			],
			resize_keyboard=True
		)
		self.keyboards["on_request_my"] = ReplyKeyboardMarkup(
			keyboard=[
				[
					KeyboardButton(text="My slots"),
					KeyboardButton(text="My info")
				],
				[
					KeyboardButton(text="My projects")
				],
			],
			resize_keyboard=True
		)
		self.texts["on_bot_command"] = "Do you want to log in?"
		self.texts["on_request_my"] = "What information do you want to receive?"

	def on_back(self, msg):
		scope = _find_router_parent(self.on_text_current, _on_text_all_routers)
		self.on_text_current = scope if scope is not None else self.on_text_current
		print("parent scope = ", scope)
		self.update_message(msg)

	def update_message(self, msg):
		type(msg)
		self.sender.sendMessage(
			self.texts[self.on_text_current],
			reply_markup=self.keyboards[self.on_text_current]
		)

	def open(self, initial_message, seed):
		type(seed)
		r = False
		if telepot.glance(initial_message)[0] == "text" \
			and _by_command(initial_message, self.on_text_current) == "start":

			self.on_start(initial_message)
			r = True
		if self.intrasess is None:
			self.on_text_current = "on_bot_command"
			self.update_message(initial_message)
			return True
		return r

	def on_start(self, msg):
		self.sender.sendMessage(
			"Now I can not help you. Because my developer is "
			"retard. And I'm doing fucking nothing at this stage."
		)
		self.sender.sendSticker("CAADAgADlAEAAr8cUgGAy_0IhJb-_AI")
		if self.intrasess is None:
			self.on_text_current = "on_bot_command"
			return self.update_message(msg)

	def on_start_code(self, msg):
		text = msg["text"]
		try:
			command, payload = text.split(' ')
			print("command = {}; payload={}".format(command, payload))
			token = self.intrasess.fetch_token(
				code=payload
			)
			type(token)
			print("Get token!")
			resp = self.intrasess.get("/v2/me")
			jsonobj = resp.json()
			log.debug(jsonobj)
			self.on_text_current = "on_request_my"
			self.sender.sendMessage(
				"Hello, {}\n".format(jsonobj["login"])
				+ self.texts[self.on_text_current],
				reply_markup=self.keyboards[self.on_text_current]
			)
		except Exception as ex:
			self.sender.sendMessage(
				"Something gone wrong!(\n"
				"Try again later or report to @dtitenko\n"
				"Sorry.("
			)
			print(ex)
			log.debug(str(ex))

	def on_new_token(self, msg):
		type(msg)
		try:
			self.intrasess = IntraSession()
			link, code = self.intrasess.authorization_url()
			keyboard = InlineKeyboardMarkup(
				inline_keyboard=[
					[InlineKeyboardButton(text="Authorize", url=link)]
				]
			)
			self.sender.sendMessage(
				"Please use this link to log in.)",
				reply_markup=keyboard
			)
		except Exception as ex:
			self.sender.sendMessage(
				"Something gone wrong!(\n"
				"Try again later or report to @dtitenko\n"
				"Sorry.("
			)
			print(ex)

	def on_request_my_slots(self, msg):
		if self.intrasess is None:
			self.on_text_current = "on_bot_command"
			return self.update_message(msg)
		resp = None
		num = 0
		sent = self.sender.sendMessage("Loading your slots...")
		self._keyboard_msg_ident = telepot.message_identifier(sent)
		self.editor = telepot.helper.Editor(self.bot, self._keyboard_msg_ident)
		self.slots_page_num = 1
		self._show_next_page()
		print("on_request_my_slots call")

	def on_request_my_info(self, msg):
		if self.intrasess is None:
			self.on_text_current = "on_bot_command"
			return self.update_message(msg)
		resp = self.intrasess.get("/v2/me")
		resp_msg = views.info(resp)
		resp_msg = emoji.emojize(resp_msg)
		self.sender.sendMessage(resp_msg, parse_mode="HTML")
		print("on_request_my_info call")

	def on_request_my_projects(self, msg):
		if self.intrasess is None:
			self.on_text_current = "on_bot_command"
			return self.update_message(msg)
		resp = self.intrasess.get("/v2/me/projects")
		# for el in resp.json():
		# 	self.sender.sendMessage(el)
		print("on_request_my_projects call")

	def on_delete_slot(self, msg):
		print("on_delete_slot")
		text = msg["text"]
		ids_ = re.findall("^/delete_slot_([0-9]{1,20})$", text)
		print("ids = ", ids_)
		if len(ids_) > 0:
			resp = self.intrasess.delete("/v2/slots/{}".format(ids_[0]))
			if resp.status_code != 204:
				self.sender.sendMessage(str(resp))

	@staticmethod
	def on_other(msg):
		print(msg)
		pass

	def on_callback_query(self, msg):
		print("I'm in on_callback_query")
		query_id, from_id, query_data = telepot.glance(
			msg, flavor='callback_query'
		)
		self.slots_page_num += 1 if query_data == "next" \
			else -1 if query_data == "prev" else 0
		self._show_next_page()

	def _show_next_page(self):
		resp = self.intrasess.get("/v2/me/slots", params={
			"page[number]": self.slots_page_num,
			"page[size]": 20
		})
		self.link = header_link_to_dict(resp)
		slots = list(filter(filter_slots, resp.json()))
		num = len(slots)
		print("slots_page_num = {}, slots = {}".format(self.slots_page_num, num))
		ik1 = []

		if "next" in self.link and num:
			resp_next = self.intrasess.get("/v2/me/slots", params={
				"page[number]": self.slots_page_num + 1,
				"page[size]": 20
			})
			if resp_next.status_code != 200:
				print(resp_next)
				return
			next_slots = list(filter(filter_slots, resp_next.json()))
			print("next_slots = ", len(next_slots))
			if len(next_slots) > 0:
				ik1.append(
					InlineKeyboardButton(text='Next page', callback_data='next')
				)
		msg, code = views.slots_page(resp, self.slots_page_num - 1)
		if code == 0:
			self.editor.editMessageText(
				msg
			)
			return
		keyboard = InlineKeyboardMarkup(
			inline_keyboard=[ik1]
		)
		self.editor.editMessageText(
			msg, parse_mode="HTML",
			reply_markup=keyboard
		)
