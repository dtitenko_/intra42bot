from pprint import pprint
import os
from requests import Response
from requests_oauthlib import OAuth2Session
from oauthlib.oauth2.rfc6749.errors import TokenExpiredError

UID = os.environ.get("UID_API_INTRA")
SECRET = os.environ.get("SECRET_API_INTRA")

_base_url = "https://api.intra.42.fr"
_token_url = "/oauth/token"
_auth_url = "/oauth/authorize"
_scopes = [
	"public",
	"projects",
	"profile",
	"tig"
]


def refresh_token(fn):
	def wraper(self=None, *args, **kwargs):
		try:
			return fn(self, *args, **kwargs)
		except TokenExpiredError:
			self.refresh_token(token_url=_base_url + _auth_url)
			return fn(self, *args, **kwargs)
	return wraper


class IntraSession(OAuth2Session):
	def __init__(
		self, client_id=UID, scope=_scopes,
		auto_refresh_url=_base_url + _token_url, *args, **kwars):

		redirect_uri = "https://intra42bot.herokuapp.com/code"
		super(IntraSession, self).__init__(
			*args, client_id=client_id, redirect_uri=redirect_uri,
			**kwars, scope=scope, auto_refresh_url=auto_refresh_url
		)

	def authorization_url(self, url=_auth_url, state=None, **kwargs):
		if not url.startswith(_base_url):
			url = _base_url + url
		authorize_url, state = super(IntraSession, self).authorization_url(
			url, state=state
		)
		return authorize_url, state

	@refresh_token
	def get(self, url, **kwargs) -> Response:
		if not url.startswith(_base_url):
			url = _base_url + url
		return super(IntraSession, self).get(
			url=url, **kwargs
		)

	@refresh_token
	def post(self, url, data=None, json=None, **kwargs) -> Response:
		if not url.startswith(_base_url):
			url = _base_url + url
		return super(IntraSession, self).post(
			url=url, data=data, json=json, **kwargs
		)

	@refresh_token
	def patch(self, url, data=None, **kwargs) -> Response:
		if not url.startswith(_base_url):
			url = _base_url + url
		return super(IntraSession, self).patch(
			url=url, data=data, **kwargs
		)

	@refresh_token
	def put(self, url: str, data=None, **kwargs) -> Response:
		if not url.startswith(_base_url):
			url = _base_url + url
		return super(IntraSession, self).put(
			url=url, data=data, **kwargs
		)

	@refresh_token
	def delete(self, url, **kwargs) -> Response:
		url = _base_url + url
		return super(IntraSession, self).delete(
			url=url, **kwargs
		)

	def fetch_token(
		self, token_url=_base_url + _token_url, client_secret=SECRET,
		code=None, authorization_response=None, body='',
		auth=None, username=None, password=None, method='POST',
		timeout=None, headers=None, verify=True, proxies=None,
		**kwargs):
		if not token_url.startswith(_base_url):
			token_url = _base_url + token_url
		token = super(IntraSession, self).fetch_token(
					token_url=token_url, client_secret=client_secret, code=code,
					authorization_response=authorization_response, body=body,
					auth=auth, username=username, password=password,
					method=method, timeout=timeout, headers=headers,
					verify=verify, proxies=proxies,
					**kwargs)
		return token

if __name__ == '__main__':
	sess = IntraSession()
	link, status = sess.authorization_url()
	pprint(link)
	_code = input("Continue?")
	_token = sess.fetch_token(code=_code)
	pprint(sess.get("/v2/me/slots").json())
