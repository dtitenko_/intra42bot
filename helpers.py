import re
def header_link_to_dict(resp):
	d = dict()
	link = resp.headers["Link"]
	l = link.split(',')
	for el in l:
		lnk, rel = el.split(";")
		rel = rel.strip()
		lnk = lnk.strip(" <").strip(">")
		k = re.findall("^rel=\"(.*)\"$", rel)[0]
		v = re.findall("^https://api\.intra\.42\.fr(.*)$", lnk)[0]
		d[k] = v
	return d