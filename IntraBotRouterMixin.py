import re
import telepot
from telepot.helper import DefaultRouterMixin, Router


def _check_on_back(msg):
	return "on_back" if msg["text"].strip() == "Back" else None


def _by_request_my(msg):
	pat = {
		"My slots": "my_slots",
		"My info": "my_info",
		"My projects": "my_projects"
	}
	try:
		text = msg["text"].strip()
		for pats, values in pat.items():
			if text == pats:
				return values
	except Exception as ex:
		print(ex)
	return None


def _by_command(msg, curr):
	patterns = [
		"^/start [a-zA-Z0-9]{64}$",
		"^/delete_slot_[0-9]{1,20}$",
		"^/start$",
		"^/new_token$",
		"^/my_slots$",
		"^/my_info$",
		"^/my_projects$",
	]
	text = msg["text"].strip()
	print("command text = ", text)
	print("delete match = ", re.match(patterns[1], text))
	if re.match(patterns[0], text):
		return "start_code"
	if re.match(patterns[1], text):
		print("match delete slot command")
		return "delete_slot"
	for pat in patterns[1:]:
		r = re.compile(pat)
		if r.match(text):
			return text[1:]
	if text == "Request new token" and curr == "on_bot_command":
		return "new_token"
	return None


def _by_text(msg, curr):
	text = msg["text"]
	if re.match("^/[a-zA-Z0-9]{1,64}", text) \
		or re.match("^on_bot_command$", curr):
		return "on_bot_command"
	else:
		return "on_request_my"

_on_text_all_routers = {
	"on_command":
	{
		"on_request_my":
		{
			"my_slots": None,
			"my_info": None,
			"my_projects": None
		}
	}
}


def _find_router_parent(name, scope):
	if isinstance(scope, dict):
		for key, value in scope.items():
			if key == name:
				return name
			child = _find_router_parent(name, value)
			if child == name:
				return key
	return None


def _find_router_childs(name, scope):
	if isinstance(scope, dict):
		for key, value in scope.items():
			if key == name:
				return value
			value = _find_router_childs(name, value)
			if value is not None:
				return value
	return None


class IntraBotRouterMixin(DefaultRouterMixin):
	def __init__(self, *args, **kwargs):
		super(IntraBotRouterMixin, self).__init__(*args, **kwargs)
		self.on_text_current = "on_bot_command"
		self.routers = dict()
		self.routers["on_request_my"] = Router(
			_by_request_my,
			{
				"my_slots":
					lambda msg: self.on_request_my_slots(msg),
				"my_info":
					lambda msg: self.on_request_my_info(msg),
				"my_projects":
					lambda msg: self.on_request_my_projects(msg),
				# "on_back": lambda msg: self.on_back(msg),
				None: lambda msg: self.on_other(msg)
			}
		)
		self.routers["on_bot_command"] = Router(
			lambda msg: _by_command(msg, self.on_text_current),
			{
				"start_code": lambda msg: self.on_start_code(msg),
				"start": lambda msg: self.on_start(msg),
				"new_token": lambda msg: self.on_new_token(msg),
				"my_slots":
					lambda msg: self.on_request_my_slots(msg),
				"my_info":
					lambda msg: self.on_request_my_info(msg),
				"my_projects":
					lambda msg: self.on_request_my_projects(msg),
				"delete_slot":
					lambda msg: self.on_delete_slot(msg),
				None: lambda msg: self.on_other(msg)
			}
		)
		self.routers["on_text"] = Router(
			lambda msg: _by_text(msg, self.on_text_current),
			{
				"on_bot_command": self.routers["on_bot_command"].route,
				"on_request_my": self.routers["on_request_my"].route,
				None: lambda msg: self.on_other(msg)
			}
		)
		self.routers["content_type"] = Router(
			lambda msg: telepot.glance(msg)[0],
			{
				'text': self.routers["on_text"].route,
				None: lambda msg: self.on_other(msg)
			}
		)
		self.router.routing_table["chat"] = self.routers["content_type"].route
