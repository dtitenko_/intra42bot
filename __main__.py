import os
import telepot

from telepot.loop import OrderedWebhook
from telepot.delegate import (
	per_chat_id, create_open, pave_event_space, include_callback_query_chat_id
)

from flask import Flask, request, redirect

from MessageHandler import MessageHandler
from SlotsCallbackHandler import SlotsCallbackHandler


TOKEN = os.environ['PP_BOT_TOKEN']
PORT = int(os.environ.get('PORT', 5000))
URL = "https://92.249.82.77" #"https://intra42bot.herokuapp.com/webhook"
CERT     = './server.crt'
CERT_KEY = './server.key'

context = (CERT, CERT_KEY)

app = Flask(__name__)

sess = telepot.helper.SafeDict()

bot = telepot.DelegatorBot(TOKEN, [
	include_callback_query_chat_id(
	pave_event_space())(
		per_chat_id(), create_open, MessageHandler, timeout=72000
	)
])

webhook = OrderedWebhook(bot)


@app.route('/code', methods=['GET', 'POST'])
def code_feeder():
	try:
		code = request.args.get('code')
	except Exception:
		code = ""
	url = "http://telegram.me/Intra42Bot?start={}".format(
		code
	)
	return redirect(url)


@app.route('/webhook', methods=['GET', 'POST'])
def pass_update():
	webhook.feed(request.data)
	return 'OK'


if __name__ == '__main__':
	try:
		bot.setWebhook(URL+":"+str(PORT)+"/webhook", certificate=open(CERT, "rb"))
	# Sometimes it would raise this error, but webhook still set successfully.
	except telepot.exception.TooManyRequestsError:
		pass

	webhook.run_as_thread()
	app.run(
		host="0.0.0.0",
		port=PORT,
		ssl_context=context,
		debug=True
	)
