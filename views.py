import os
from jinja2 import Environment, FileSystemLoader, select_autoescape

from IntraSession import IntraSession
from slots import (filter_slots, filter_slots_by_days)

THIS_DIR = os.path.dirname(os.path.abspath(__file__))
env = Environment(
	loader=FileSystemLoader(os.path.join(THIS_DIR, "templates")),
	autoescape=select_autoescape(['html', 'xml']),
	trim_blocks=True
)


def slots_page(resp, day_offset=0, *args, **kwargs):
	if resp is None:
		return "Sorry, but you have no slots for this period", 0
	if day_offset < 0 or day_offset > 13:
		return "Invalid day offset"
	template = env.get_template("my_slots.html")
	if resp.status_code != 200:
		return resp.json(), 0
	slots = resp.json()
	slots = list(filter(filter_slots, slots))
	print(slots)
	if len(slots) == 0:
		return "Sorry, but you have no slots for this period", 0
	print("num slots = ", len(slots))
	return template.render(slots=slots), 1


def info(resp, *args, **kwargs):
	template = env.get_template("profile_info.html")
	if resp.status_code != 200:
		return resp.json()
	info = resp.json()
	cursus = resp.json()["cursus_users"]
	return template.render(info=info, cursus=cursus)



if __name__ == '__main__':
	print(os.path.join(THIS_DIR, "templates"))
