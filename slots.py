import datetime, dateutil.parser


def filter_slots(obj: dict):
	end_at = dateutil.parser.parse(obj["end_at"])
	now = datetime.datetime.now(datetime.timezone.utc)
	return end_at >= now


def filter_slots_by_days(obj: dict, day_offset=0):
	if not filter_slots(obj):
		return False
	day_off = datetime.timedelta(days=day_offset)
	begin_at = dateutil.parser.parse(obj["begin_at"])
	date = datetime.datetime.now(datetime.timezone.utc) + day_off
	return date.isocalendar() == begin_at.isocalendar()
