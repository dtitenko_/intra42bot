import telepot
from telepot.namedtuple import(
	CallbackQuery, InlineKeyboardButton, InlineKeyboardMarkup
)
from telepot.delegate import (
	per_chat_id, per_callback_query_origin, create_open, pave_event_space
)
import re
from IntraSession import IntraSession
import views
from slots import filter_slots_by_days

def header_link_to_dict(resp):
	d = dict()
	link = resp.headers["Link"]
	l = link.split(',')
	for el in l:
		lnk, rel = el.split(";")
		rel = rel.strip()
		lnk = lnk.strip(" <").strip(">")
		k = re.findall("^rel=\"(.*)\"$", rel)[0]
		v = re.findall("^https://api\.intra\.42\.fr(.*)$", lnk)[0]
		d[k] = v
	return d

class SlotsCallbackHandler(telepot.helper.CallbackQueryOriginHandler):
	def __init__(self, *args, **kwargs):
		super(SlotsCallbackHandler, self).__init__(*args, **kwargs)
		self.page_num = 1
		self.link = dict()
		self.session = None

	def on_callback_query(self, msg):
		query_id, from_id, query_data = telepot.glance(
			msg, flavor='callback_query'
		)
		if query_data == "next":
			self.page_num += 1
		else:
			self.page_num -= 1
		self._show_next_page()

	def _show_next_page(self):
		resp = self.session.get("/v2/me/slots", params={
			"page[number]": self.page_num,
			"page[size]": 20
		})
		if resp.status_code != 200:
			return
		self.link = header_link_to_dict(resp)
		num = list(
			filter(
				lambda obj: filter_slots_by_days(obj, self.page_num-1),
				resp.json()
			)
		)
		ik1 = []
		if "prev" in self.link and num:
			ik1.append(
				InlineKeyboardButton(text='Prev page', callback_data='prev')
			)
		else:
			self.page_num += 1
		if "next" in self.link and num:
			ik1.append(
				InlineKeyboardButton(text='Next page', callback_data='next')
			)
		else:
			self.page_num -= 1
		msg, code = views.slots_page(resp, self.page_num - 1)
		keyboard = InlineKeyboardMarkup(
			inline_keyboard=[ik1]
		)
		self.editor.editMessageText(
			msg,
			reply_markup=keyboard
		)

